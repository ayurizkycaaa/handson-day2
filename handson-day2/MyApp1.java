import java.util.Scanner;

import PackageStudent.Student;

public class MyApp1 {
    public static void main(String[] args) {
        String data;
        Student student = new Student();
        Scanner keyboard = new Scanner(System.in);
        System.out.print("Ketik nama, email,& no.HP : ");
        data = keyboard.nextLine();

        String[] arrOfData = data.split(" ");

        student.setName(arrOfData[0]);
        student.setEmail(arrOfData[1]);
        student.setPhoneNumber(arrOfData[2]);

        System.out.println();
        System.out.println("Student : ");
        System.out.println("student.getName() : " + student.getName());
        System.out.println("student.getEmail() : " + student.getEmail());
        System.out.println("student.getPhoneNumber() : " + student.getPhoneNumber()); // mengakses variabel statik menggunakan nama classnya
    }
}
