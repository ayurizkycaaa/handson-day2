import java.util.Scanner;
import PackageStudent.Student;

public class MyApp{
    public static void main(String[] args) {
        // String a, b, c;
        Student student = new Student();
        Scanner keyboard = new Scanner(System.in);
        System.out.print("Ketik nama : ");
        student.setName(keyboard.nextLine());
        System.out.print("Ketik email : ");
        student.setEmail(keyboard.nextLine());
        System.out.print("Ketik no.HP : ");
        student.setPhoneNumber(keyboard.nextLine());

        System.out.println();
        System.out.println("Student : ");
        System.out.println("student.getName() : " + student.getName());
        System.out.println("student.getEmail() : " + student.getEmail());
        System.out.println("student.getPhoneNumber() : " + student.getPhoneNumber()); // mengakses variabel statik menggunakan nama classnya
    }
}
